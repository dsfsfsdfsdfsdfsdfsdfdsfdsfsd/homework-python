def dif(list1, list2):
    new_list = set(list1).difference(set(list2))
    return list(new_list)


print(dif([1, 2, 3], [3, 4, 5, 2]))

class Animal:
    def __init__(self, name, age):
        self.name = name
        self.age = age


class Cat(Animal):
    def __init__(self, name, lastname, age):
        super().__init__(name, age)
        self.lastname = lastname

    def voice(self):
        print("Meow meow")

    def __str__(self):
        return f"{self.name} is {self.age} years old"


cat = Cat("Barsik", "Barsikov", 5)
print(cat.name)
cat.voice()
print(cat)

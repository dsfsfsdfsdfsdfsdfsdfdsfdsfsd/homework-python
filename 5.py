from random import randint

d = {}
max_value = []
keys = []
n = int(input("Введите количество чисел: "))
for i in range(n):
    d[i + 1] = randint(1, 100)

print("Полученный словарь:")
print(d)

for k, v in sorted(d.items(), key=lambda x: x[1]):
    if v not in max_value:
        max_value.append(v)

new_max_value = max_value[-3:]
print(new_max_value)

for i in new_max_value:
    for k, v in d.items():
        if i == v and i not in keys:
            keys.append(k)

print("Ключи с максимальными значениями:", *keys)

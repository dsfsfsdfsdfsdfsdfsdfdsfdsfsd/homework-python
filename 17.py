class Cat:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def voice(self):
        print("Meow meow")


cat = Cat("Barsik", 3)
print(cat.name)
cat.voice()
